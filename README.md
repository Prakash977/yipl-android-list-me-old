# ListMe App #

This application lists users, posts, albums from the fake api using retrofit. Also, retrofit caching is implemented so that the application can display the data's when in offline mode.

### Screenshots ###

<img src="images/users.png" width="150" height="200">
<img src="images/posts.png" width="150" height="200">