package com.example.listme.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.listme.R;
import com.example.listme.adapter.PhotoAdapter;
import com.example.listme.api.ApiBuilder;
import com.example.listme.api.JsonPlaceHolderApi;
import com.example.listme.model.Photo;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumPhotosActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView photosDetailsRecyclerView;

    private List<Photo> photosDetails;
    private PhotoAdapter photoAdapter;

    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_photos);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        progressBar = findViewById(R.id.progress_bar);
        photosDetailsRecyclerView = findViewById(R.id.photosDetailsRecyclerView);
        photosDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        String album_id = getIntent().getStringExtra("album_id");

        //visibility set to Visible
        progressBar.setVisibility(View.VISIBLE);

        ApiBuilder apiBuilder = new ApiBuilder();
        apiBuilder.retrofitBuilder().create(JsonPlaceHolderApi.class)
                .getPhotosDetails(album_id)
                .enqueue(new Callback<List<Photo>>() {
                    @Override
                    public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                        photosDetails = response.body();
                        photoAdapter = new PhotoAdapter(photosDetails);
                        photosDetailsRecyclerView.setAdapter(photoAdapter);

                        //visibility set to gone
                        progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(Call<List<Photo>> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();

                        //visibility set to gone
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }
}