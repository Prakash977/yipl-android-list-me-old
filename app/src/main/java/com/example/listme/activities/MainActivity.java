package com.example.listme.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import android.annotation.SuppressLint;
import android.os.Bundle;
import com.example.listme.R;
import com.example.listme.fragments.AlbumsFragment;
import com.example.listme.fragments.PostsFragment;
import com.example.listme.fragments.UsersFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(onItemSelected);

        if (savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new UsersFragment())
                    .commit();
        }
    }


    @SuppressLint("NonConstantResourceId")
    private final BottomNavigationView.OnNavigationItemSelectedListener onItemSelected = item -> {
        Fragment selectedFragment = null;

        switch (item.getItemId()){
            case R.id.users:
                selectedFragment = new UsersFragment();
                break;

            case R.id.posts:
                selectedFragment = new PostsFragment();
                break;

            case R.id.albums:
                selectedFragment = new AlbumsFragment();
                break;
        }

        assert selectedFragment != null;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, selectedFragment)
                .commit();
        return true;
    };
}