package com.example.listme.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.listme.R;
import com.example.listme.adapter.CommentAdapter;
import com.example.listme.api.ApiBuilder;
import com.example.listme.api.JsonPlaceHolderApi;
import com.example.listme.model.Comments;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostCommentsActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView commentsRecyclerView;

    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_comments);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        progressBar = findViewById(R.id.progress_bar);
        commentsRecyclerView = findViewById(R.id.commentsRecyclerView);
        commentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        String post_id = getIntent().getStringExtra("post_id");

        //visibility set to Visible
        progressBar.setVisibility(View.VISIBLE);

        ApiBuilder apiBuilder = new ApiBuilder();
        apiBuilder.retrofitBuilder().create(JsonPlaceHolderApi.class)
                .getPostComments(post_id)
                .enqueue(new Callback<List<Comments>>() {
            @Override
            public void onResponse(Call<List<Comments>> call, Response<List<Comments>> response) {
                CommentAdapter commentAdapter = new CommentAdapter(getApplicationContext(), response.body());
                commentsRecyclerView.setAdapter(commentAdapter);

                //visibility set to gone
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<List<Comments>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();

                //visibility set to gone
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}