package com.example.listme.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.listme.R;
import com.example.listme.adapter.UserAlbumsAdapter;
import com.example.listme.api.ApiBuilder;
import com.example.listme.api.JsonPlaceHolderApi;
import com.example.listme.api.OnItemClickListener;
import com.example.listme.model.Albums;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserAlbumsActivity extends AppCompatActivity implements OnItemClickListener {
    private Toolbar toolbar;
    private RecyclerView albumsRecyclerView;
    private List<Albums> albumsList;

    private UserAlbumsAdapter albumAdapter;

    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_albums);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        progressBar = findViewById(R.id.progress_bar);
        albumsRecyclerView = findViewById(R.id.albumsRecyclerView);
        albumsRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        String user_id = getIntent().getStringExtra("user_id");

        //visibility set to Visible
        progressBar.setVisibility(View.VISIBLE);

        ApiBuilder apiBuilder = new ApiBuilder();
        apiBuilder.retrofitBuilder().create(JsonPlaceHolderApi.class)
                .getUserAlbums(user_id)
                .enqueue(new Callback<List<Albums>>() {
                    @Override
                    public void onResponse(Call<List<Albums>> call, Response<List<Albums>> response) {
                        albumsList = response.body();
                        albumAdapter = new UserAlbumsAdapter(albumsList, UserAlbumsActivity.this);
                        albumsRecyclerView.setAdapter(albumAdapter);

                        //visibility set to gone
                        progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(Call<List<Albums>> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();

                        //visibility set to gone
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onViewPostsClick(int position) {

    }

    @Override
    public void onViewAlbumsClick(int position) {

    }

    @Override
    public void onItemClick(int position) {
        String album_id = albumsList.get(position).getId();

        Intent intent = new Intent(getApplicationContext(), AlbumPhotosActivity.class);
        intent.putExtra("album_id", album_id);
        startActivity(intent);
    }
}