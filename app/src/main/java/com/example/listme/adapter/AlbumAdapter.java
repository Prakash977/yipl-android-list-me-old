package com.example.listme.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.listme.R;
import com.example.listme.model.Albums;
import com.google.android.material.card.MaterialCardView;
import java.util.List;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {
    private final List<Albums> albumsList;


    public AlbumAdapter(List<Albums> albumsList) {
        this.albumsList = albumsList;
    }


    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {
        Albums getPosition = albumsList.get(position);

        holder.title.setText(getPosition.getTitle());
    }

    @Override
    public int getItemCount() {
        return albumsList.size();
    }

    public static class AlbumViewHolder extends RecyclerView.ViewHolder{
        private TextView title, bodyText, body;
        private MaterialCardView item_layout;
        View divider;


        public AlbumViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);

            //Removing visibility of body text from the layout since album section doesn't include body
            bodyText = itemView.findViewById(R.id.subTitleText);
            body = itemView.findViewById(R.id.subTitle);
            bodyText.setVisibility(View.GONE);
            body.setVisibility(View.GONE);
            divider = itemView.findViewById(R.id.divider);
            divider.setVisibility(View.GONE);

            item_layout = itemView.findViewById(R.id.item_layout);
            item_layout.setOnClickListener(v -> {});
        }
    }
}
