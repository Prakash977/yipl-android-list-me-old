package com.example.listme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.listme.R;
import com.example.listme.model.Comments;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {
    private Context context;
    private List<Comments> commentList;


    public CommentAdapter(Context context, List<Comments> commentList) {
        this.context = context;
        this.commentList = commentList;
    }


    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_item_layout, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        Comments getPosition = commentList.get(position);

        holder.name.setText(getPosition.getName());
        holder.email.setText(getPosition.getEmail());
        holder.body.setText(getPosition.getBody());
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder{
        private TextView name, email, body;
        private MaterialCardView item_layout;

        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.email);
            body = itemView.findViewById(R.id.body);

            item_layout = itemView.findViewById(R.id.item_layout);
            item_layout.setOnClickListener(v -> {});
        }
    }
}
