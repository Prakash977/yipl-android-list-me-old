package com.example.listme.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.listme.R;
import com.example.listme.model.Photo;
import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;
import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {
    private List<Photo> photoDetails;


    public PhotoAdapter(List<Photo> photoDetails) {
        this.photoDetails = photoDetails;
    }


    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photos_item_layout, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        Photo getPosition = photoDetails.get(position);

        Picasso.get().load(getPosition.getUrl()).into(holder.image_layout);
        holder.title.setText(getPosition.getTitle());
    }

    @Override
    public int getItemCount() {
        return photoDetails.size();
    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_layout;
        private TextView title;
        private MaterialCardView item_layout;


        public PhotoViewHolder(@NonNull View itemView) {
            super(itemView);

            image_layout = itemView.findViewById(R.id.image_layout);
            title = itemView.findViewById(R.id.title);

            item_layout = itemView.findViewById(R.id.item_layout);
            item_layout.setOnClickListener(v -> {});
        }
    }
}
