package com.example.listme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.listme.R;
import com.example.listme.model.Posts;
import com.google.android.material.card.MaterialCardView;
import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {
    private Context context;
    private List<Posts> postsList;


    public PostAdapter(Context context, List<Posts> postsList) {
        this.context = context;
        this.postsList = postsList;
    }


    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        Posts getPosition = postsList.get(position);

        holder.title.setText(getPosition.getTitle());
        holder.body.setText(getPosition.getBody());
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder{
        private final TextView title, body;
        private MaterialCardView item_layout;

        public PostViewHolder(View itemView){
            super(itemView);

            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.subTitle);
            item_layout = itemView.findViewById(R.id.item_layout);
            item_layout.setOnClickListener(v -> {});
        }
    }
}
