package com.example.listme.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.example.listme.R;
import com.example.listme.api.OnItemClickListener;
import com.example.listme.model.Albums;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import java.util.List;

public class UserAlbumsAdapter extends RecyclerView.Adapter<UserAlbumsAdapter.AlbumViewHolder> {
    private final List<Albums> albumsList;
    private final OnItemClickListener onItemClickListener;


    public UserAlbumsAdapter(List<Albums> albumsList, OnItemClickListener onItemClickListener) {
        this.albumsList = albumsList;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {
        Albums getPosition = albumsList.get(position);

        holder.title.setText(getPosition.getTitle());
    }

    @Override
    public int getItemCount() {
        return albumsList.size();
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView title, bodyText, body;

        private MaterialCardView item_layout;
        private ConstraintLayout dropdown_option_layout;
        private MaterialButton viewBtn;

        View divider;

        public AlbumViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            divider = itemView.findViewById(R.id.divider);
            divider.setVisibility(View.GONE);

            //Removing visibility of body text from the layout since album section doesn't include body
            bodyText = itemView.findViewById(R.id.subTitleText);
            body = itemView.findViewById(R.id.subTitle);
            bodyText.setVisibility(View.GONE);
            body.setVisibility(View.GONE);

            dropdown_option_layout = itemView.findViewById(R.id.dropdown_option_layout);
            viewBtn = itemView.findViewById(R.id.viewBtn);
            viewBtn.setOnClickListener(this);

            item_layout = itemView.findViewById(R.id.item_layout);
            item_layout.setOnClickListener(v -> {
                if (dropdown_option_layout.getVisibility() == View.GONE){
                    dropdown_option_layout.setVisibility(View.VISIBLE);
                    viewBtn.setText("View Photos");
                } else {
                    dropdown_option_layout.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }
}
