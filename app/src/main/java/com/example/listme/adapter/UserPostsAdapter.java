package com.example.listme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.example.listme.R;
import com.example.listme.api.OnItemClickListener;
import com.example.listme.model.Posts;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import java.util.List;

public class UserPostsAdapter extends RecyclerView.Adapter<UserPostsAdapter.PostViewHolder> {
    private Context context;
    private final List<Posts> postsList;

    OnItemClickListener onItemClickListener;

    public UserPostsAdapter(Context context, List<Posts> postsList, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.postsList = postsList;
        this.onItemClickListener = onItemClickListener;
    }


    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        Posts getPosition = postsList.get(position);

        holder.title.setText(getPosition.getTitle());
        holder.body.setText(getPosition.getBody());
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final TextView title, body;

        private MaterialCardView item_layout;
        private ConstraintLayout dropdown_option_layout;
        private MaterialButton viewBtn;


        public PostViewHolder(View itemView){
            super(itemView);

            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.subTitle);

            dropdown_option_layout = itemView.findViewById(R.id.dropdown_option_layout);
            viewBtn = itemView.findViewById(R.id.viewBtn);
            viewBtn.setOnClickListener(this);

            item_layout = itemView.findViewById(R.id.item_layout);
            item_layout.setOnClickListener(v -> {
                if (dropdown_option_layout.getVisibility() == View.GONE){
                    dropdown_option_layout.setVisibility(View.VISIBLE);
                    viewBtn.setText("View Comments");
                } else {
                    dropdown_option_layout.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }
}
