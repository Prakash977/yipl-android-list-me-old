package com.example.listme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.example.listme.R;
import com.example.listme.api.OnItemClickListener;
import com.example.listme.model.Users;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersViewHolder> {
    private Context context;
    private List<Users> listUsers;

    private OnItemClickListener onItemClickListener;


    public UsersAdapter(Context context, List<Users> listUsers, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.listUsers = listUsers;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_item_layout, parent, false);

        return new UsersViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersViewHolder holder, int position) {
        Users getPosition = listUsers.get(position);

        holder.userName.setText(getPosition.getName());

        String address = getPosition.getAddress().getStreet() + ", " + getPosition.getAddress().getCity() + ", " + getPosition.getAddress().getZipcode();
        holder.address.setText(address);
    }

    @Override
    public int getItemCount() {
        return listUsers == null ? 0 : listUsers.size();
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder{
        private MaterialCardView item_layout;
        private TextView userName, address;
        private ImageView dropdown_option;
        private ConstraintLayout dropdown_option_layout;

        private MaterialButton viewPostsBtn, viewAlbumsBtn;

        OnItemClickListener onItemClickListener;

        public UsersViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            this.onItemClickListener = onItemClickListener;

            userName = itemView.findViewById(R.id.title);
            address = itemView.findViewById(R.id.subTitle);

            dropdown_option = itemView.findViewById(R.id.dropdown_option);
            dropdown_option_layout = itemView.findViewById(R.id.dropdown_option_layout);

            item_layout = itemView.findViewById(R.id.item_layout);
            item_layout.setOnClickListener(v -> {
                if (dropdown_option_layout.getVisibility() == View.GONE){
                    dropdown_option_layout.setVisibility(View.VISIBLE);
                    dropdown_option.setImageResource(R.drawable.ic_baseline_arrow_up);
                } else {
                    dropdown_option_layout.setVisibility(View.GONE);
                    dropdown_option.setImageResource(R.drawable.ic_baseline_arrow_down);
                }
            });


            //on button View Posts clicked its data will be fetched on next screen
            viewPostsBtn = itemView.findViewById(R.id.viewPostsBtn);
            viewPostsBtn.setOnClickListener(v -> onItemClickListener.onViewPostsClick(getAdapterPosition()));

            //on button View Albums clicked its data will be fetched on next screen
            viewAlbumsBtn = itemView.findViewById(R.id.viewAlbumsBtn);
            viewAlbumsBtn.setOnClickListener(v -> onItemClickListener.onViewAlbumsClick(getAdapterPosition()));
        }
    }
}
