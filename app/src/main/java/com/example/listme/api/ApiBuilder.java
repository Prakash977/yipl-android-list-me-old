package com.example.listme.api;

import android.util.Log;
import com.example.listme.MyApplication;
import java.io.File;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiBuilder {
    private static final String TAG = "ApiBuilder";
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_PRAGMA = "Pragma";

    private static final String BASE_URL = "https://jsonplaceholder.typicode.com/";


    private Retrofit retrofit = null;

    private static final long cacheSize = 5 * 1024 * 1024; //5MB


    public Retrofit retrofitBuilder(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    private OkHttpClient okHttpClient() {
        return new OkHttpClient.Builder()
                .cache(cache())
                .addInterceptor(httpLoggingInterceptor())       // used if network off or on
                .addNetworkInterceptor(networkInterceptor())    // used only if network is available
                .addInterceptor(offlineInterceptor())           // used if offline
                .build();
    }

    private Cache cache() {
        return new Cache(new File(MyApplication.getInstance().getCacheDir(), "offlineCache"), cacheSize);
    }

    private HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(s -> Log.d(TAG, "http log: "+s));

        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return httpLoggingInterceptor;
    }


    /**
     * This interceptor will be called ONLY if the network is available
     */
    private Interceptor networkInterceptor() {
        return chain -> {
            Log.d(TAG, "intercept: Network interceptor called.");

            Response response = chain.proceed(chain.request());

            CacheControl cacheControl = new CacheControl.Builder()
                    .maxAge(5, TimeUnit.SECONDS)
                    .build();

            return response.newBuilder()
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                    .build();
        };
    }


    /**
     * This interceptor will be called both if the network is available and if the network is not available
     */
    private Interceptor offlineInterceptor() {
        return chain -> {
            Log.d(TAG, "intercept: Offline Interceptor called.");

            Request request = chain.request();

            if (!MyApplication.hasNetwork()){
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build();

                request = request.newBuilder()
                        .removeHeader(HEADER_PRAGMA)
                        .removeHeader(HEADER_CACHE_CONTROL)
                        .cacheControl(cacheControl)
                        .build();
            }
            return chain.proceed(request);
        };
    }
}
