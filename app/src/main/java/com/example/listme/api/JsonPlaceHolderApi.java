package com.example.listme.api;

import com.example.listme.model.Albums;
import com.example.listme.model.Comments;
import com.example.listme.model.Photo;
import com.example.listme.model.Posts;
import com.example.listme.model.Users;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface JsonPlaceHolderApi {
    @GET("users")
    Call<List<Users>> getUsers();

    @GET("posts")
    Call<List<Posts>> getPosts();

    @GET("albums")
    Call<List<Albums>> getAlbums();

    @GET("users/{user_id}/posts")
    Call<List<Posts>> getUserPosts(@Path("user_id") String user_id);

    @GET("posts/{post_id}/comments")
    Call<List<Comments>> getPostComments(@Path("post_id") String post_id);

    @GET("users/{user_id}/albums")
    Call<List<Albums>> getUserAlbums(@Path("user_id") String user_id);

    @GET("albums/{album_id}/photos")
    Call<List<Photo>> getPhotosDetails(@Path("album_id") String album_id);
}
