package com.example.listme.api;

public interface OnItemClickListener {
    void onViewPostsClick(int position);

    void onViewAlbumsClick(int position);

    void onItemClick(int position);
}
