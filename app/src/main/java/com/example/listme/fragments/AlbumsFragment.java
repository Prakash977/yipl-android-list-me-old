package com.example.listme.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.listme.R;
import com.example.listme.adapter.AlbumAdapter;
import com.example.listme.api.ApiBuilder;
import com.example.listme.api.JsonPlaceHolderApi;
import com.example.listme.model.Albums;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumsFragment extends Fragment {
    private RecyclerView albumsRecyclerView;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle("Albums");

        return inflater.inflate(R.layout.fragment_albums, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        albumsRecyclerView = view.findViewById(R.id.albumsRecyclerView);
        albumsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        //visibility set to Visible
        progressBar.setVisibility(View.VISIBLE);

        ApiBuilder apiBuilder = new ApiBuilder();
        apiBuilder.retrofitBuilder().create(JsonPlaceHolderApi.class)
                .getAlbums()
                .enqueue(new Callback<List<Albums>>() {
                    @Override
                    public void onResponse(Call<List<Albums>> call, Response<List<Albums>> response) {
                        AlbumAdapter albumAdapter = new AlbumAdapter(response.body());
                        albumsRecyclerView.setAdapter(albumAdapter);

                        //visibility set to gone
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<List<Albums>> call, Throwable t) {
                        Toast.makeText(getContext(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();

                        //visibility set to gone
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }
}