package com.example.listme.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.listme.R;
import com.example.listme.activities.UserAlbumsActivity;
import com.example.listme.activities.UserPostsActivity;
import com.example.listme.adapter.UsersAdapter;
import com.example.listme.api.ApiBuilder;
import com.example.listme.api.JsonPlaceHolderApi;
import com.example.listme.api.OnItemClickListener;
import com.example.listme.model.Users;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UsersFragment extends Fragment implements OnItemClickListener {
    private static final String TAG = "UsersFragment";

    private RecyclerView recyclerView;
    private UsersAdapter adapter;

    private List<Users> usersList;

    private ProgressBar progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle("Users");

        return inflater.inflate(R.layout.fragment_users, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        recyclerView = view.findViewById(R.id.usersRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        //visibility set to Visible
        progressBar.setVisibility(View.VISIBLE);
        ApiBuilder apiBuilder = new ApiBuilder();
        apiBuilder.retrofitBuilder().create(JsonPlaceHolderApi.class)
                .getUsers()
                .enqueue(new Callback<List<Users>>() {
                    @Override
                    public void onResponse(Call<List<Users>> call, Response<List<Users>> response) {
                        usersList = response.body();
                        Log.d(TAG, "onResponse: "+response);

                        adapter = new UsersAdapter(getContext(), usersList, UsersFragment.this);
                        recyclerView.setAdapter(adapter);

                        //visibility set to gone
                        progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(Call<List<Users>> call, Throwable t) {
                        Toast.makeText(getContext(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();

                        //visibility set to gone
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }



    @Override
    public void onViewPostsClick(int position) {
        String user_id = usersList.get(position).getId();

        Intent intent = new Intent(getContext(), UserPostsActivity.class);
        intent.putExtra("user_id", user_id);
        startActivity(intent);
    }

    @Override
    public void onViewAlbumsClick(int position) {
        String user_id = usersList.get(position).getId();

        Intent intent = new Intent(getContext(), UserAlbumsActivity.class);
        intent.putExtra("user_id", user_id);
        startActivity(intent);
    }

    @Override
    public void onItemClick(int position) {
    }
}