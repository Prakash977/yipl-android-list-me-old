package com.example.listme.model;

public class Users {
    private String id;
    private String name;
    private Address address;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public static class Address {
        private String zipcode;
        private String city;
        private String street;

        public String getZipcode() {
            return zipcode;
        }

        public String getCity() {
            return city;
        }

        public String getStreet() {
            return street;
        }
    }
}
